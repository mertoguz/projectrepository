SELF-PRINTING T-SHIRT E-TICARET SITESI 
(tshirtturlu.com)

FURKAN MERT OĞUZ

B151210019

C GRUBU



Standart e-ticaret sitelerinden farklı olarak, ürünlerin kullanıcılar tarafından sisteme konulup, onaylandığı süre boyunca bizzat site tarafından basımı yapıldığı ve diğer kullanıcıya satıldığı, hem ürünü koyan üyenin hem de sitenin kazandığı bir e-ticaret sitesi.

Genel bakışı e-ticaret sitesinden farksız ve üyelerin direkt olarak self-printing uygulamasına dahil olmadan da normal bir müşteri gibi alışveriş yapabileceği site.

İsteyen üyelerin self-printing uygulamasına dahil olmasıyla bu üyeler, kendi tasarladıkları t-shirt baskılarını site onayına göndererek satılan her ürünlerinden %10 gelir elde edebileceklerdir.

Tasarlanan baskılar, ürün penceresinde yapay bir öngörü özelliği sayesinde görüntülenebilecektir.

Zamanla talep alan ürünlerde küçük fiyat artışlarını sistem otomatik olarak gerçekleştirir. Bu sayede hem site, hem de ürünü tasarlayan üye kazanır.

Üyelerin gelir limiti bulunmaktadır, limit dolduğunda %10 luk pay dağıtımı durdurulur. Kullanıcı limitsiz hesap için bir miktar ödeme yaparak pay almaya devam edebilir. 

►Kullanıcıların üye olmadan önce de yapabilecekleri :
- Ürünleri görmek ve incelemek.
- Ürün satın alabilmek.
- Ürünlere yapılan yorumları görebilmek

►Kullanıcıların üye olduktan sonra yapabilecekleri 
- Ürün oluşturup site onayına göndermek.
- Satın aldığı ürünlerin inceleme sayfalarına yorum yazabilmek.
- Kendi ürününe yazılan yorumlara yanıt verebilmek.
- Panoları satın alıp kendi tasarımlarını öne çıkarabilmek.
- Takipçi kazanabilmek ya da takip edebilmek.
