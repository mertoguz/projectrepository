//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseMe
{
    using System;
    
    public enum productRank : int
    {
        unRanked = 0,
        Copper = 1,
        Silver = 2,
        Gold = 3,
        EditorSpecial = 4
    }
}
