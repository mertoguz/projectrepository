﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebProject.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "UserName")]
        public string username { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string password { get; set; }

    }

}